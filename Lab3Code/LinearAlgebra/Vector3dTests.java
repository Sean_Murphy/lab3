//Sean Murphy 1935620

package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

public class Vector3dTests {
    
    @Test
    public void testGetMethods(){
        Vector3d vector = new Vector3d(2, 4, 6);
        assertEquals(2, vector.getX());
        assertEquals(4, vector.getY());
        assertEquals(6, vector.getZ());
    }

    @Test
    public void testMagnitude(){
        Vector3d vector = new Vector3d(2, 4, 6);
        assertEquals(Math.sqrt(56), vector.magnitude());
    }

    @Test
    public void testDotProduct(){
        Vector3d vector1 = new Vector3d(2, 4, 6);
        Vector3d vector2 = new Vector3d(3, 5, 7);
        assertEquals(68, vector1.dotProduct(vector2));
    }

    @Test
    public void testAdd(){
        Vector3d vector1 = new Vector3d(2, 4, 6);
        Vector3d vector2 = new Vector3d(3, 5, 7);
        Vector3d vectorSum = vector1.add(vector2);
        assertEquals(5, vectorSum.getX());
        assertEquals(9, vectorSum.getY());
        assertEquals(13, vectorSum.getZ());
    }
}
